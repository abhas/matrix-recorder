# Changelog for Matrix Recorder

## Changes in v0.0.6 (2017-10-18)

- recorder-to-html now allows to only export a time segment (thanks to dvoitsek, #7)
- Bumped matrix-js-sdk to 0.7.13 (breaking changes in 0.8 have to wait)

## Changes in v0.0.5 (2017-09-28)

- Bugfix: Homeserver URL should allow port numbers


## Changes in v0.0.4 (2017-09-02)

- Bugfixes against time outs and for better media downloading
- Amended the README to note that you need to overwrite the hard-coded timeout
  in matrix-js-sdk if you want to do a large initial sync


## Changes in v0.0.3 (2017-06-05)

- Updated matrix-js-sdk to 0.7.10 with external olm library


## Changes in v0.0.2 (2017-02-06)

- Will now display device key (for verification) after initial sync


## Changes in v0.0.1 (2017-01-31)

- Initial release
